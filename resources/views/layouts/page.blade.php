<!DOCTYPE html>
<html>
	<link rel="stylesheet" type="text/css" href="css/style.css">
    <head>
        <title>Transi-app</title>
    </head>
    <body >
       <header>
	       <div>
		       	<ul class="header">
		       		<li><img class="logo-empresa" src="images/devmov5.png"></li>
		       		<li>Añadir Ruta</li>
		       		<li><img class="logo-app" src="images/Transiapp.png"></li>
		       	</ul>
	       	</div>
       </header>
       <div class="page">
       		<div class="slider">
       			<ul>
       				<li>Añadir Ruta</li>
       				<li>Ver Rutas</li>
       				<li>Añadir Bus</li>
       				<li>Ver Buses</li>
       				<li>Administar Tarifas</li>
       			</ul>
       		</div>
       		<div class="content">
       			@yield('content')
       		</div>
       </div>
    </body>

</html>
