<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gestion extends Model
{
    protected $table = 'gestiones';
    protected $fillable = ['ID_ADMINISTRADOR','ID_RUTA','FECHA_GESTION'];
}
