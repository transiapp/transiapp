<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class relevo extends Model
{
    protected $table = 'relevos';
    protected $fillable = ['ID_ADMINISTRADOR','ID_EMPRESA','FECHA_RELEVO'];
}
