<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class administrador extends Model
{
    protected $table = 'administradores';
    protected $fillable = ['ID_ADMINISTRADOR','NOMBRE_CUENTA','CONTRASENA','NOMBRE_ADMINISTRADOR','APELLIDO_ADMINISTRADOR','NUMERO_IDENTIFICACION'];
}
