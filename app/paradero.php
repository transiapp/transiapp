<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paradero extends Model
{
    protected $table = 'paraderos';
    protected $fillable = ['ID_PARADERO','ID_POSICION','NOMBRE_PARADERO'];
}
