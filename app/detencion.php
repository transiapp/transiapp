<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detencion extends Model
{
    protected $table = 'detenciones';
    protected $fillable = ['ID_PARADERO','ID_BUS','TIEMPO_DETENIDO'];
}
