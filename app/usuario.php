<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey = 'ID_USUARIO';

    public function posicion(){
    	return $this->belongsToMany('App\posicion');
    }
}
