<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bus extends Model
{
    protected $table = 'buses';
    protected $fillable = ['ID_BUS','CAPACIDAD','ID_GPS','MARCA','PLACA'];
}
