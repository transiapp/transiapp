<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rodamiento extends Model
{
    protected $table = 'rodamientos';
    protected $fillable = ['ID_BUS','ID_RUTA','FECHA_SALIDA'];
}
