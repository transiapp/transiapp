<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posicion extends Model
{
    protected $table = 'posiciones';
    protected $fillable = ['ID_POSICION','ID_BUS','ID_USUARIO','ID_PARADERO','LATITUD','LONGITUD','NOMBRE_POSICION'];
    public function usuario(){
    	return $this->hasOne('App\usuario');
    }
}
