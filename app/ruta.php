<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ruta extends Model
{
    protected $table = 'rutas';
    protected $primaryKey = 'ID_RUTA';
    protected $fillable = ['ID_POSICION','POS_ID_PODICION','NOMBRE_RUTA','HORA_INICIO','HORA_FINAL','TARIFA','DESCRIPCION'];

 	public function posicionOrigen(){
 		return $this->hasOne('App\posicion');
 	}
 	public function posicionLlegada(){
 		return $this->hasOne('App\posicion');	
 	}
 	public function rodamiento(){
 		return $this->;
 	}
}
